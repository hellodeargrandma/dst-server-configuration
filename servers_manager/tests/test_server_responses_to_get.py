import logging
import socket
import threading
import time
import urllib
from collections import namedtuple
from urllib import request

import pytest

from servers_status_aggregator import StoppableHTTPServer

ServerAddress = namedtuple("ServerAddress", ["url", "port"])


def wait_for_server(server_addr: ServerAddress, timeout: int = 5) -> bool:
    until = time.time() + timeout
    while time.time() < until:
        try:
            # TOOD: replace with sending header only
            resp = request.urlopen(
                f"{server_addr.url}:{server_addr.port}", timeout=0.001
            )
            if resp.status == 200:
                return True
        except urllib.error.URLError:
            ...
        except socket.timeout:
            ...

    return False


@pytest.fixture(autouse=True, scope="session")
def servers_manager_server():
    server_addr = ServerAddress(url="http://127.0.0.1", port=8085)

    def run_server():
        logging.getLogger("http").setLevel("INFO")
        nonlocal server_addr

        server = StoppableHTTPServer(("", server_addr.port))
        try:
            server.run()
        except KeyboardInterrupt:
            print("Server stopped.")

    print("Starting server")
    thread = threading.Thread(target=run_server)
    thread.start()
    if not wait_for_server(server_addr):
        raise Exception("Unable to start server.")
    print("Server started.")

    # Return server and wait till the end of test session to stop it.
    yield server_addr

    print("Sending 'stop' to server.")
    resp = request.urlopen(
        f"{server_addr.url}:{server_addr.port}?command=stop"
    )
    if resp.status == 200:
        print("Server got command.")
    thread.join()


def test_send_empty_request(servers_manager_server):
    url = f"{servers_manager_server.url}:{servers_manager_server.port}?data"
    resp = request.urlopen(url)
    assert resp.status == 200


def test_send_data(servers_manager_server):
    data = "%5B%7B%22players%22%3A%5B%7B%22name%22%3A%22%5BHost%5D%22%2C%22admin%22%3Atrue%2C%22userid%22%3A%22KU%5FzRuzxb6o%22%2C%22prefab%22%3A%22%22%2C%22age%22%3A0%7D%5D%2C%22mods%22%3A%5B%22workshop%2D462469447%22%2C%22workshop%2D347079953%22%2C%22workshop%2D485345553%22%2C%22workshop%2D764204839%22%2C%22workshop%2D623749604%22%2C%22workshop%2D362175979%22%2C%22workshop%2D2087177552%22%2C%22workshop%2D503330921%22%2C%22workshop%2D356420397%22%2C%22workshop%2D2038507668%22%2C%22workshop%2D2324689937%22%2C%22workshop%2D363112314%22%2C%22workshop%2D375859599%22%2C%22workshop%2D1462979419%22%2C%22workshop%2D466732225%22%2C%22workshop%2D378160973%22%2C%22workshop%2D604761020%22%5D%2C%22statevars%22%3A%7B%22summerlength%22%3A15%2C%22cavemoonphase%22%3A%22quarter%22%2C%22iscavefullmoon%22%3Afalse%2C%22isnightmaredawn%22%3Afalse%2C%22elapseddaysinseason%22%3A3%2C%22isfullmoon%22%3Afalse%2C%22moisture%22%3A3081%2E8371582031%2C%22cavephase%22%3A%22dusk%22%2C%22iscavewaxingmoon%22%3Afalse%2C%22isnightmarewild%22%3Afalse%2C%22nightmaretimeinphase%22%3A0%2C%22precipitationrate%22%3A0%2E55942809536946%2C%22iswet%22%3Afalse%2C%22isnewmoon%22%3Afalse%2C%22precipitation%22%3A%22rain%22%2C%22israining%22%3Atrue%2C%22isnight%22%3Afalse%2C%22isdusk%22%3Atrue%2C%22time%22%3A0%2E66995204289754%2C%22temperature%22%3A11%2E328098617566%2C%22wetness%22%3A30%2E03338432312%2C%22isspring%22%3Atrue%2C%22moistureceil%22%3A3646%2E0559082031%2C%22isday%22%3Afalse%2C%22issummer%22%3Afalse%2C%22remainingdaysinseason%22%3A17%2C%22winterlength%22%3A15%2C%22isalterawake%22%3Afalse%2C%22isnightmarecalm%22%3Afalse%2C%22timeinphase%22%3A0%2E81703324090867%2C%22cycles%22%3A38%2C%22iscavedusk%22%3Atrue%2C%22isautumn%22%3Afalse%2C%22issnowing%22%3Afalse%2C%22moonphase%22%3A%22quarter%22%2C%22seasonprogress%22%3A0%2E15%2C%22snowlevel%22%3A0%2C%22issnowcovered%22%3Afalse%2C%22autumnlength%22%3A20%2C%22pop%22%3A1%2C%22nightmaretime%22%3A0%2C%22phase%22%3A%22dusk%22%2C%22iswaxingmoon%22%3Afalse%2C%22nightmarephase%22%3A%22none%22%2C%22springlength%22%3A20%2C%22iscaveday%22%3Afalse%2C%22iscavenight%22%3Afalse%2C%22iscavenewmoon%22%3Afalse%2C%22season%22%3A%22spring%22%2C%22iswinter%22%3Afalse%2C%22isnightmarewarn%22%3Afalse%7D%2C%22settings%22%3A%7B%22description%22%3A%22This+server+is+made+by+Roma+Lera+and+Alex%22%2C%22maxplayers%22%3A6%2C%22clanid%22%3A%22%22%2C%22session%22%3A%224B8CFD56DB09598C%22%2C%22adminonline%22%3Afalse%2C%22servername%22%3A%22LeraAlexRoma%5Fserver%22%2C%22clanonly%22%3Afalse%2C%22gamemode%22%3A%22endless%22%2C%22mods%22%3Atrue%2C%22friendsonly%22%3Afalse%2C%22dedicated%22%3Atrue%2C%22passworded%22%3Atrue%2C%22pvp%22%3Afalse%7D%2C%22world%22%3A%7B%22overrides%22%3A%5B%5D%7D%7D%5D"
    url = f"{servers_manager_server.url}:{servers_manager_server.port}?data={data}"
    resp = request.urlopen(url)
    assert resp.status == 200
