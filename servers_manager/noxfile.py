import nox

@nox.session()
def tests(session):
    session.install(".[tests]")

    session.run("pytest", *session.posargs)

@nox.session()
def lint(session):
    session.notify("lint_black")
    session.notify("lint_isort")
    session.notify("lint_flake8")

@nox.session()
def lint_black(session):
    session.install("black")
    session.run("black", *session.posargs, "src/", "tests/")

@nox.session()
def lint_isort(session):
    session.install("isort")
    session.run("isort", "src/", "tests/")

@nox.session()
def lint_flake8(session):
    session.install("flake8")
    session.run("flake8", "src/", "tests/")

