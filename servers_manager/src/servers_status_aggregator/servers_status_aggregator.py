#!/usr/bin/env python3
"""
Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]
"""
import json
import logging
from http.server import BaseHTTPRequestHandler, HTTPServer
from sys import argv
from urllib.parse import parse_qs, urlparse


class HTTPRequestsHandler(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def do_GET(self):
        query = parse_qs(urlparse(self.path).query)
        logging.info("GET request\nHeaders:\n{}\n".format(str(self.headers)))

        data = query.get("data")
        if data:
            data_json = json.loads(data[0])
            logging.info(
                "Path:\n{}\n".format(
                    json.dumps(data_json, indent=4),
                )
            )
        command = query.get("command")
        if command and command[0] == "stop":
            self._set_response()
            self.wfile.write("ok".encode("utf-8"))
            logging.info("Stopping server")
            self.server.running = False

        self._set_response()
        self.wfile.write("ok".encode("utf-8"))

    def log_request(self, code="-", size="-"):
        if not logging.getLogger("http").isEnabledFor(logging.INFO):
            return

        return super().log_request(code, size)


class StoppableHTTPServer(HTTPServer):
    def __init__(
        self,
        server_address=("", 8080),
        RequestHandlerClass=HTTPRequestsHandler,
    ):
        self.running = True
        super().__init__(server_address, RequestHandlerClass)

    def run(self):
        logging.basicConfig(level=logging.INFO)

        logging.info("Я родился!!!\n")
        self.serve_forever()
        self.server_close()
        logging.info("Stopped httpd...\n")

    def serve_forever(self):
        while self.running:
            self.handle_request()


def main():
    logging.getLogger("http").setLevel("INFO")

    port = 8080
    if len(argv) == 2:
        port = int(argv[1])

    server = StoppableHTTPServer(("", port))
    try:
        server.run()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
