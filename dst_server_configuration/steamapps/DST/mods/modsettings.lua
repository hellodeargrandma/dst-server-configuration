-- Use the "ForceEnableMod" function when developing a mod. This will cause the
-- game to load the mod every time no matter what, saving you the trouble of
-- re-enabling it from the main menu.
--
-- Note! You shout NOT do this for normal mod loading. Please use the Mods menu
-- from the main screen instead.

--ForceEnableMod("kioskmode_dst")

-- Use "EnableModDebugPrint()" to show extra information during startup.

--EnableModDebugPrint()

-- Use "EnableModError()" to make the game more strict and crash on bad mod practices.

--EnableModError()

-- Use "DisableModDisabling()" to make the game stop disabling your mods when the game crashes

--DisableModDisabling()

-- Use "DisableLocalModWarning()" to make the game stop warning you when enabling local mods.

--DisableLocalModWarning()


--#No Thermal Stone Durability
EnableModDebugPrint("466732225")
--#No More Respawn Penalty
EnableModDebugPrint("356420397")
--#No More Meatatarian
EnableModDebugPrint("485345553")
--#Map Revealer for DST
EnableModDebugPrint("363112314")
--#Map Discovery Sharing
EnableModDebugPrint("462469447")
--#Large Chest (HD)
EnableModDebugPrint("2087177552")
--#More equip slots
EnableModDebugPrint("2324689937")
--#Extra Equip Slots
EnableModDebugPrint("375850593")
--#DJPaul's Sort Inventory (repost, fix)
EnableModDebugPrint("1462979419")
--#Global Positions
EnableModDebugPrint("378160973")
--#Item Info
--ServerModSetup("1901927445")
--#Automatic Health Adjust
EnableModDebugPrint("764204839")
--#Health Info
EnableModDebugPrint("375859599")
--#Multi Rocks
EnableModDebugPrint("604761020")
--#Combined Status
--ServerModSetup("376333686")
--#[DST] Storeroom
EnableModDebugPrint("623749604")
--#Display Food Values
EnableModDebugPrint("347079953")
--#Extended Indicators WIP
--ServerModSetup("358749986")
--#Wormhole Marks [DST]
EnableModDebugPrint("362175979")
--#Minimap HUD
--ServerModSetup("345692228")

