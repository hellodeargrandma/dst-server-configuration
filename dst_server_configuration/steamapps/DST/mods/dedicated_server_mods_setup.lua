--There are two functions that will install mods, ServerModSetup and
--ServerModCollectionSetup. Put the calls to the functions in this file and
--they will be executed on boot.

--ServerModSetup takes a string of a specific mod's Workshop id. It will download and install the mod to your mod directory on boot.
	--The Workshop id can be found at the end of the url to the mod's Workshop page.
	--Example: http://steamcommunity.com/sharedfiles/filedetails/?id=350811795
	--ServerModSetup("350811795")

--ServerModCollectionSetup takes a string of a specific mod's Workshop id. It will download all the mods in the collection and install them to the mod directory on boot.
	--The Workshop id can be found at the end of the url to the collection's Workshop page.
	--Example: http://steamcommunity.com/sharedfiles/filedetails/?id=379114180
	--ServerModCollectionSetup("379114180")

--#No Thermal Stone Durability
ServerModSetup("466732225")
--#No More Respawn Penalty
ServerModSetup("356420397")
--#No More Meatatarian
ServerModSetup("485345553")
--#Map Revealer for DST
ServerModSetup("363112314")
--#Map Discovery Sharing
ServerModSetup("462469447")
--#Large Chest (HD)
ServerModSetup("2087177552")
--#More equip slots
ServerModSetup("2324689937")
--#Extra Equip Slots
ServerModSetup("375850593")
--#DJPaul's Sort Inventory (repost, fix)
ServerModSetup("1462979419")
--#Global Positions
ServerModSetup("378160973")
--#Automatic Health Adjust
ServerModSetup("764204839")
--#Health Info
ServerModSetup("375859599")
--#Multi Rocks
ServerModSetup("604761020")
--#[DST] Storeroom
ServerModSetup("623749604")
--#Display Food Values
ServerModSetup("347079953")
--#Wormhole Marks [DST]
ServerModSetup("362175979")
--#Server Status
ServerModSetup("503330921")

