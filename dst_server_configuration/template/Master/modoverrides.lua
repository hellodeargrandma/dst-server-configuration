return {
	--#No Thermal Stone Durability
	["workshop-466732225"] = { enabled = true },
	--#No More Respawn Penalty
	["workshop-356420397"] = { enabled = true },
	--#No More Meatatarian
	["workshop-485345553"] = { enabled = true },
	--#Map Revealer for DST
	["workshop-363112314"] = { enabled = true },
	--#Map Discovery Sharing
	["workshop-462469447"] = { enabled = true },
	--#Large Chest (HD)] = { enabled = true },
	["workshop-2087177552"] = { enabled = true },
	--#More equip slots
	["workshop-2324689937"] = { enabled = true },
	--#DJPaul's Sort Inventory (repost, fix)
	["workshop-1462979419"] = { enabled = true },
	--#Global Positions
	["workshop-378160973"] = { enabled = true },
	--#Automatic Health Adjust
	["workshop-764204839"] = { enabled = true },
	--#Health Info
	["workshop-375859599"] = { enabled = true },
	--#Multi Rocks
	["workshop-604761020"] = { enabled = true },
	--#[DST] Storeroom
	["workshop-623749604"] = { enabled = true },
	--#Display Food Values
	["workshop-347079953"] = { enabled = true },
	--#Wormhole Marks [DST]
	["workshop-362175979"] = { enabled = true },
	--#Server Status
	["workshop-503330921"] = {
		enabled = true,
		configuration_options = {
			API_URL = "http://localhost:8080?data=",
			API_URL_EXTRA = "http://localhost:8080?data=",
			Send_Password = false,
		}
	}
	--#Extended Indicators WIP
	--["workshop-358749986"] = { enabled = false },
	--#Extra Equip Slots (doesn't allow to wear backpack with logsuit)
	--["workshop-375850593"] = { enabled = false },
}
